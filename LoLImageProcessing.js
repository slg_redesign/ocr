var microsoftAPI = require('microsoft-computer-vision');
var fs = require('fs');
var im = require('imagemagick');
var jpeg = require('jpeg-js');


function processImage(res){
    var heightPercent = 0.0625;
    //im.convert(['./LoL/Screenshot.png', './LoL/Screenshot.jpg'], function(err,stdout){
        im.identify('./LoL/Screenshot.png', function(err, features){
            if(err) throw err;
            var width = features.width;
            var height = features.height;
            var newHeight = height*heightPercent;
            im.crop({
                srcPath: './LoL/Screenshot.png',
                dstPath: './LoL/test.png',
                width: width,
                height: newHeight,
                quality: 1,
                gravity: "North"
            }, function(err, stdout){
                //res.send("done");
                im.crop({
                    srcPath: './LoL/test.png',
                    dstPath: './LoL/test.png',
                    width: width*0.49,
                    height: newHeight,
                    quality: 1,
                    gravity: "West"
                }, function(err, stdout){
                    //res.send("done");
                    im.crop({
                        srcPath: './LoL/test.png',
                        dstPath: './LoL/test.png',
                        width: (width*0.49)*0.36,
                        height: newHeight,
                        quality: 1,
                        gravity: "East"
                    }, function(err, stdout){
                        im.convert(['./LoL/test.png', './LoL/test.jpg'], function(err, stdout){
                            //res.send("done");
                            fs.readFile('./LoL/test.jpg', function(err, data){
                                var rawImageData = jpeg.decode(data);

                                var rawFrameData = getTestFrameData(rawImageData);

                                var jpegImageData = jpeg.encode(rawFrameData, 50);

                                fs.writeFile('LoLProcessedTest.png', jpegImageData.data, function(err){
                                    res.send("done");
                                    // microsoftAPI.orcImage({
                                    //     "Ocp-Apim-Subscription-Key": "ff4773cc53004fb3b77c6f247c7cf25e",
                                    //     "request-origin":"westcentralus",
                                    //     "detect-orientation":false,
                                    //     "language":"en",
                                    //     "content-type":"application/octet-stream",
                                    //     "body":jpegImageData.data
                                    // }).then((result) =>{
                                    //     var less = result.regions;
                                    //     res.json(less);
                                    // })
                                });
                            })
                        })
                    })
                })
            })
        })
    //})
}




function getTestFrameData(rawImageData){
    //build 2d array from image data;
    var topArray = [];
    for(var i =0; i<rawImageData.data.length; i+=0){
        var lowerArray = [];
        for(var j=0; j<4; j++){
            if(j==3){
                topArray.push(lowerArray);
                i++;
            }
            else{
                lowerArray[j]=rawImageData.data[i];
                i++;
            }
        }
    }

    //collect all points where RBG is within threshhold and set to 255, all other points 0
    // var rMin = 20;
    // var rMax = 190;
    // var gMin = 85;
    // var gMax = 256;
    // var bMin = 160;
    // var bMax = 256;
    var rMin = 0;
    var rMax = 200;
    var gMin = 55;
    var gMax = 256;
    var bMin = 70;
    var bMax = 256;
    var frameData = new Buffer(rawImageData.width * rawImageData.height * 4);
    var frameDataIndex =0;
    for(var i=0; i<topArray.length; i++){
        //check if this RBG aray is within threshhold
        //white = names
        //blue = bottom player win/clan tag
        //light pink = top player win
        //dark pink = top player clan tag
        if((
                (topArray[i][0]>rMin)  //check for blue text
            &&  (topArray[i][0]<rMax)
            &&  (topArray[i][1]>gMin)
            &&  (topArray[i][1]<gMax)
            &&  (topArray[i][2]>bMin)
            &&  (topArray[i][2]<bMax)
        //    )||(
        //         (topArray[i][0]>89)  //check for blue
        //     &&  (topArray[i][0]<113)
        //     &&  (topArray[i][1]>gMin-2)
        //     &&  (topArray[i][2]>bMin-2)
        //    )||(
        //         (topArray[i][0]>rMin)  //check for light pink
        //     &&  (topArray[i][0]<rMax)
        //     &&  (topArray[i][1]>197)
        //     &&  (topArray[i][1]<212)
        //     &&  (topArray[i][2]>bMin)
        //     &&  (topArray[i][2]<bMax)
           )){
            frameData[frameDataIndex++] = 0xFF;
            frameData[frameDataIndex++] = 0xFF;
            frameData[frameDataIndex++] = 0xFF;
            frameData[frameDataIndex++] = 0xFF;
        }
        else{
            frameData[frameDataIndex++] = 0x00;
            frameData[frameDataIndex++] = 0x00;
            frameData[frameDataIndex++] = 0x00;
            frameData[frameDataIndex++] = 0xFF;
        }
    }

    //re-encode data to jpeg
    var rawFrameData = {
        data: frameData,
        width: rawImageData.width,
        height: rawImageData.height
    };

    return rawFrameData;
}

module.exports.processImage = processImage;
