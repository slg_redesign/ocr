process.on('message', function(m){
    var s1 = process.argv[2];
    var s1len = process.argv[3];
    var s2 = process.argv[4];
    var s2len = process.argv[5];
    var dist = ldist(s1, s1len, s2, s2len);
    var response = {string:s2,
                    dist:dist};
    process.send(response);
});

function ldist(s1, s1len, s2, s2len){
    var cost;
    if(s1len == 0) return s1len;
    if(s2len == 0) return s2len;

    if(s1[s1len-1] == s2[s2len-1]){
        cost =0;
    }
    else{
        cost=1;
    }
    return Math.min( ldist(s1,s1len-1,s2,s2len)+1,
                     ldist(s1,s1len,s2,s2len-1)+1,
                     ldist(s1,s1len-1,s2,s2len-1)+cost);
}
