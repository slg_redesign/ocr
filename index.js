var express = require('express');
var http = require('http');
var im  = require('imagemagick');
var imageProcessing = require('./imageProcessingAsync');
var lolImageProcessing = require('./lolImageProcessing');
var rlImageProcessing = require('./rlImageProcessing');
var fs = require('fs');


var app = express(),
    server = http.createServer(app),
    port = process.env.PORT || 1234;

server.listen(port, function(){
    console.log('Sever listening on given port 1234')
});

app.get('/', function(req,res){
    var result = imageProcessing.processImage(req.query.fileName, res);
    //res.json(result);
});

app.get('/lolCrop', function(req, res){
    lolImageProcessing.processImage(res);
});

app.get('/rlCrop', function(req, res){
    rlImageProcessing.processImage(res);
})

app.get('/testBatch', function(req, res){
    imageProcessing.testBatch();
    res.send("Running.  See console");
});





app.get('/cropTest', function(req, res){
    //var fileName = req.query.fileName;
    var width;
    var height;
    var newHeight;

    //crop percentages   //FOR TOP PLAYER 0.33,0.30; FOR BOTTOM PLAYER 0.64,0.40
    var heightPercent = 0.17;
    var newHeightPercent = 0.45;


    im.convert(['./ClashRoyale/clash_image1.png','./Originals/test.jpg'],function(err, stdout){
        im.identify('./Originals/test.jpg', function(err, features){
            if(err) throw err;
            width = features.width;
            height = features.height;
            im.crop({
                srcPath: './Originals/test.jpg',
                dstPath: './Crops/test.jpg',
                width: width,
                height: height*heightPercent,
                quality: 1,
                gravity: "North"
            }, function(err, stdout){
                im.identify('./Crops/test.jpg', function(err, features){
                    newHeight = features.height;
                    im.crop({
                        srcPath: './Crops/test.jpg',
                        dstPath: './Crops/test.jpg',
                        width: width,
                        height: newHeight*newHeightPercent,
                        quality: 1,
                        gravity: "South"
                    } , function(err, stdout){
                        if(err) throw err;
                        res.send("done");
                    })
                })
            })
        })
    })

    // res.send("done");
})

app.get('/ldist', function(req, res){
    var s1 = "abcd1234";
    var s2 = "abcd1234·12345";
    var s1len = s1.length;
    var s2len = s2.length;
    var ldist = imageProcessing.ldist(s1, s1len, s2, s2len)
    res.send("Distance: "+ldist);
});

app.get('/makeList', function(req, res){
    fs.readFile('./ClashRoyale/names.json', function(err,data){
        var correctJson = JSON.parse(data);
        var names = [];
        for(var i=0; i<correctJson.images.length; i++){
            var nameToAdd = correctJson.images[i].topPlayer;
            addName(nameToAdd, names);
            nameToAdd = correctJson.images[i].bottomPlayer;
            addName(nameToAdd, names);
        }
        var data = JSON.stringify(names);
        fs.writeFile('./ClashRoyale/allNames.json', data, function(err){
            res.send("Done");
        })
    })
})

app.get('/tryWorker', function(req, res){
    var cp = require('child_process');
    // var maxNumberOfChildren = 10;
    // var childResponses = [];
    //
    // for(var i =0; i<10; i++){
    //     var child = cp.fork('./ldist');
    //     child.send(i+': Please up-case this string');
    //     var numResponses= 0;
    //     child.on('message', function(m){
    //         childResponses.push(m);
    //         numResponses++;
    //         if(numResponses==maxNumberOfChildren){
    //             console.log(childResponses);
    //         }
    //     })
    // }
    var child = cp.fork('./ldist', ['hello' ,'world']);
    child.send('Execute');
    child.on('message', function(m){
        console.log(m);
    })

    res.send("see console");
})

function addName(nameToAdd, names){
    var alreadyExists = false;
    for(var j=0; j<names.length; j++){
        if(names[j]==nameToAdd){
            alreadyExists = true;
        }
    }
    if(!alreadyExists){
        names.push(nameToAdd);
    }
}

// app.get('/ocr', function(req, res){
//     //load cropped image binary
//     fs.readFile('./Processed1Crop.jpg', function(err, data){
//         //call ORC
//         microsoftAPI.orcImage({
//             "Ocp-Apim-Subscription-Key": "ff4773cc53004fb3b77c6f247c7cf25e",
//             "request-origin":"westcentralus",
//             "language":"en",
//             "detect-orientation":false,
//             "content-type":"application/octet-stream",
//             "body":data
//         }).then((result) =>{
//             //get the name field out of the returned data
//             var less = result.regions;
//             res.json(less);
//             // var name = '';
//             // if(less[0].lines.length<=2){
//             //     name = less[0].lines[0].words[0].text;
//             // }
//             // else{
//             //     name = less[0].lines[1].words[0].text;
//             // }
//             // name = name.toLowerCase();
//             // res.send(name);
//
//
//             // var numLines = less[0].lines.length;
//             // var name = less[0].lines[numLines-2].words[0].text;
//             // name = name.toLowerCase();
//             // res.send(name);
//         });
//     });
// })
//
// app.get('/ocr2', function(req, res){
//     //load cropped image binary
//     fs.readFile('./Untitled.jpg', function(err, data){
//         console.log(data);
//         //call ORC
//         microsoftAPI.orcImage({
//             "Ocp-Apim-Subscription-Key": "ff4773cc53004fb3b77c6f247c7cf25e",
//             "request-origin":"westcentralus",
//             "language":"en",
//             "detect-orientation":false,
//             "content-type":"application/octet-stream",
//             "body":data
//         }).then((result) =>{
//             //get the name field out of the returned data
//             var less = result.regions;
//             res.json(less);
//         });
//     });
// })
//
// app.get('/binary', function(req, res){
//     fs.readFile('./Originals/Org1.jpg', function(err, data){
//         //console.log(data);
//
//
//
//         //get the index where all of the RGB values > 250;
//         var whiteIndexs = [];
//         var binaryArray = [];
//
//         // var numHits = 0;
//         // var maxConsecHits = 0;
//
//
//         console.log(jpegImageData);
//         microsoftAPI.orcImage({
//             "Ocp-Apim-Subscription-Key": "ff4773cc53004fb3b77c6f247c7cf25e",
//             "request-origin":"westcentralus",
//             "language":"en",
//             "detect-orientation":false,
//             "content-type":"application/octet-stream",
//             "body":jpegImageData.data
//         }).then((result) =>{
//             //get the name field out of the returned data
//             var less = result.regions;
//             res.json(less);
//         });
//
//
//         // console.log("Hits: "+numHits);
//         // //longest horizontal streak
//         // console.log("maxConsecHits: "+maxConsecHits);
//         //
//         // console.log("width: "+rawImageData.width);
//         // console.log("height: "+rawImageData.height);
//         //
//         // console.log('580591/width(607)= vertical position = ' + parseInt(580591/607));
//         // console.log("580591%width(607)= horizontal position = " + (580591%607));
//
//     })
// })
//
//
// app.get('/readFile', function(req,res){
//     fs.readFile('./Originals/Org1.jpg', function(err, data){
//         console.log(data);
//         fs.readFile('./Originals/Org2.jpg', function(err, data){
//             console.log(data);
//             fs.readFile('./Originals/Org3.jpg', function(err, data){
//                 console.log(data);
//                 fs.readFile('./Untitled.jpg', function(err, data){
//                     console.log(data);
//                     console.log("Just one:"+data[0]);
//                     for(var i=0; i<1000; i++){
//                         console.log(data[i].toString(16));
//                     }
//                     console.log("done");
//                     for(var i=data.length-1; i>data.length-101; i--){
//                         console.log(data[i].toString(16));
//                     }
//                 })
//             })
//
//         })
//     })
//
//
//
// })
