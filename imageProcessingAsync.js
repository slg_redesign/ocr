var microsoftAPI = require('microsoft-computer-vision');
var fs = require('fs');
var im = require('imagemagick');
var jpeg = require('jpeg-js');
var cp = require('child_process');
var terminate = require('terminate');
'use strict'

var cropPercent1 = [0.64, 0.34, 0.17, 0.09];
var cropPercent2 = [0.40, 0.30, 0.45, 0.50];



var functionCounter = 0;
var compareCounter = 0;

var draw = false;
var topPlayerString ="";
var bottomPlayerString ="";
var winnerString = "";
var topMiss = 0;
var botMiss = 0;
var winMiss = 0;
const numBatch = 49;
var uniqueIndex = 0;
var done = false;

function testBatch(){
    done = false;
    topPlyerString = "";
    bottomPlayerString = "";
    winnerString = "";
    draw = false;
    var inputFile = './ClashRoyale/clash_image'+uniqueIndex+'.png';
    //console.log("processing "+inputFile);
    //console.log("uniqueIndex = "+uniqueIndex);
    processImage(inputFile, true, uniqueIndex);
    uniqueIndex++;
    spin();
    //console.log("numBatch = " + numBatch);
    // if(uniqueIndex<numBatch){
    //     //console.log("waiting")
    //     setTimeout(testBatch, 40000);
    // }
}

function spin(){
    if(!done){
        setTimeout(spin, 15000);
    }
    else{
        if(uniqueIndex<numBatch){
            testBatch();
        }
    }
}

function processImage(inputFile, errorCounting = false, i=0){
    var imageIndex = i;
    var fileName = inputFile;
    console.time("processImage");
    //make 4 crops for bottom player, top player, top player winner, draw
    console.log("converting "+fileName+" to "+'./Originals/test'+imageIndex+'.jpg')
    im.convert([fileName, './Originals/test'+imageIndex+'.jpg'],function(err, stdout){
        //console.log("Hello world?"+stdout);
        for(var suffix =0; suffix<3; suffix++){
            callOCR(suffix, errorCounting, imageIndex);
        }
    })
}

function callOCR(suffix, errorCounting, imageIndex){
    var index = suffix;
    //crop percentages
    var heightPercent = cropPercent1[index]; //FOR TOP PLAYER 0.33,0.30; FOR BOTTOM PLAYER 0.64,0.40; FOR TOP "WINNER" 0.13, 0.30; FOR "DRAW" 0.09, 0.50
    var newHeightPercent = cropPercent2[index];

    //Crop input image
    //console.log("Cropping: "+'./Originals/test'+imageIndex+'.jpg');
    im.identify('./Originals/test'+imageIndex+'.jpg', function(err, features){
        if(err) throw err;
        var width = features.width;
        var height = features.height;
        var bottomHeight = height*heightPercent;
        var topHeight = bottomHeight - (height*heightPercent*newHeightPercent);
        var heightDiff = bottomHeight-topHeight;
        var cropParam = ''+width+'x'+heightDiff+'+0+'+topHeight;
        console.log("Crop params for "+imageIndex+":"+index+" = "+cropParam);
        var newHeight = height*heightPercent;
        im.convert(['./Originals/test'+imageIndex+'.jpg', '-crop', cropParam, './Crops/test'+imageIndex+'_'+index+'.jpg'], function(err, stdout){
            if(err) throw err;
            fs.readFile('./Crops/test'+imageIndex+'_'+index+'.jpg', function(err, data){
                //decode jpeg
                //console.log("Decoding image "+imageIndex);
                var rawImageData = jpeg.decode(data);

                var rawFrameData = getFrameData(rawImageData);

                var jpegImageData = jpeg.encode(rawFrameData, 50);

                fs.writeFile('./ProcessedImages/ProcessedTest'+imageIndex+'_'+index+'.jpg', jpegImageData.data, function(err){
                    if(err) throw err;
                    microsoftAPI.orcImage({
                        "Ocp-Apim-Subscription-Key": "ff4773cc53004fb3b77c6f247c7cf25e",
                        "request-origin":"westcentralus",
                        "detect-orientation":false,
                        "language":"en",
                        "content-type":"application/octet-stream",
                        "body":jpegImageData.data
                    }).then((result) =>{
                        //postprocessing logic
                        //console.log(JSON.stringify(result));
                        var less = result.regions;
                        // console.log("About to switch on suffix: "+index);
                        switch (index) {
                            case 0:
                            //var stringLess = JSON.stringify(less);
                            //console.log(stringLess);
                                if(less.length!=0){
                                    //logic to set bottom player string and maybe winnerString
                                    if(less[0].lines[0].words[0].text[less[0].lines[0].words[0].text.length-1] == '!'){
                                        for(var i =0; i<less[0].lines[1].words.length; i++){
                                            //console.log("Setting bottom string on "+imageIndex);
                                            bottomPlayerString += (less[0].lines[1].words[i].text.toLowerCase() + " ");
                                            //console.log("Bottom string is "+bottomPlayerString);
                                        }
                                        if(bottomPlayerString[bottomPlayerString.length-1]==" "){
                                            //console.log("removing white space: bottom string on "+imageIndex);
                                            bottomPlayerString = bottomPlayerString.slice(0,bottomPlayerString.length-1); //cut off trailing whitespace
                                        }
                                        //console.log("setting winner string on "+imageIndex);
                                        winnerString = "Bottom";

                                    }
                                    else{
                                        for(var i =0; i<less[0].lines[0].words.length; i++){
                                            //console.log("Setting bottom string on "+imageIndex);
                                            bottomPlayerString += (less[0].lines[0].words[i].text.toLowerCase() + " ");
                                            //console.log("Bottom string is "+bottomPlayerString);
                                        }
                                        if(bottomPlayerString[bottomPlayerString.length-1]==" "){
                                            //console.log("removing white space: bottom string on "+imageIndex);
                                            bottomPlayerString = bottomPlayerString.slice(0,bottomPlayerString.length-1); //cut off trailing whitespace
                                            //console.log("After cut bottomPlayerString is "+bottomPlayerString);
                                        }
                                    }
                                }
                                else{
                                    //console.log("Setting bottom string on "+imageIndex);
                                    bottomPlayerString = "Unknown name";
                                }
                                functionCounter++;
                                if(functionCounter>2){
                                    //console.log("Calling finish from bot");
                                    finish(errorCounting, imageIndex);
                                }
                                break;
                            case 1:
                                if(less.length!=0){
                                    //logic to set top player string
                                    for(var i =0; i<less[0].lines[0].words.length; i++){
                                        topPlayerString += (less[0].lines[0].words[i].text.toLowerCase() + " ");
                                        //console.log("Setting top string on "+imageIndex+" to: "+topPlayerString);
                                    }
                                    if(topPlayerString[topPlayerString.length-1]==" "){
                                        //console.log("removing white space: top string on "+imageIndex);
                                        topPlayerString = topPlayerString.slice(0,topPlayerString.length-1); //cut off trailing whitespace
                                    }
                                }
                                else{
                                    topPlayerString = "Unknown name";
                                    //console.log("Setting top string on "+imageIndex+" to: "+topPlayerString);
                                }
                                functionCounter++;
                                if(functionCounter>2){
                                    //console.log("Calling finish from top");
                                    finish(errorCounting, imageIndex);
                                }
                                break;
                            case 2:
                                //console.log(JSON.stringify(result));
                                //logic to set winnerString
                                if(less.length!=0){
                                    console.log("initial string "+less[0].lines[0].words[0].text);
                                    var winStringMatch = sift4simple(less[0].lines[0].words[0].text, "WINNER!");
                                    console.log("distance to win string "+winStringMatch);
                                    var drawStringMatch = sift4simple(less[0].lines[0].words[0].text, "DRAW");
                                    console.log("distance to draw string "+drawStringMatch);

                                    if(winStringMatch>drawStringMatch){
                                        draw = true;
                                    }
                                    else{
                                        winnerString = "Top";
                                    }
                                }
                                functionCounter++;
                                if(functionCounter>2){
                                    finish(errorCounting, imageIndex);
                                }
                                break;
                            // case 3:
                            //
                            //     //logic to see if draw;
                            //     if(less.length!=0){
                            //         console.log("setting draw "+imageIndex);
                            //         draw=true;
                            //     }
                            //     functionCounter++;
                            //     if(functionCounter>3){
                            //         finish(errorCounting, imageIndex);
                            //     }
                            //     break;
                            default:
                                //res.send("An error occured");

                        }
                    });
                });
            });
        })
    })
}

function finish(errorCounting, imageIndex){
    //console.log("Entering Finish.  Top is "+topPlayerString);
    if(draw){
        //console.log("Build draw json on "+imageIndex);
        var resultJson = {
                            "topPlayer":topPlayerString,
                            "bottomPlayer":bottomPlayerString,
                            "winner":"Draw"
                        };
        //console.log("clearing values on "+imageIndex);
        topPlayerString = "";
        bottomPlayerString = "";
        winnerString = "";
        draw = false;
        functionCounter= 0;
        if(errorCounting){
            compareToJson(resultJson, imageIndex);
        }else{
            //res.json(resultJson);
        }
    }
    else{
        // if(winnerString == "Top"){
        //     winnerString = "Top";
        // }
        //console.log("Build json on "+imageIndex);
        var resultJson = {
                            "topPlayer":topPlayerString,
                            "bottomPlayer":bottomPlayerString,
                            "winner":winnerString
                        };
        //console.log("clearing values on "+imageIndex);
        topPlayerString = "";
        bottomPlayerString = "";
        winnerString = "";
        draw = false;
        functionCounter=0;
        if(errorCounting){
            compareToJson(resultJson, imageIndex);
        }
        else{
            //res.json(resultJson);
        }
    }
}


function getFrameData(rawImageData){
    //build 2d array from image data;
    var topArray = [];
    for(var i =0; i<rawImageData.data.length; i+=0){
        var lowerArray = [];
        for(var j=0; j<4; j++){
            if(j==3){
                topArray.push(lowerArray);
                i++;
            }
            else{
                lowerArray[j]=rawImageData.data[i];
                i++;
            }
        }
    }

    //collect all points where RBG is within threshhold and set to 255, all other points 0
    var rMin = 240;
    var rMax = 256;
    var gMin = 240;
    var gMax = 256;
    var bMin = 240;
    var bMax = 256;
    var frameData = new Buffer(rawImageData.width * rawImageData.height * 4);
    var frameDataIndex =0;
    for(var i=0; i<topArray.length; i++){
        //check if this RBG aray is within threshhold
        //white = names
        //blue = bottom player win/clan tag
        //light pink = top player win
        //dark pink = top player clan tag
        if((
                (topArray[i][0]>rMin)  //check for white
            &&  (topArray[i][0]<rMax)
            &&  (topArray[i][1]>gMin)
            &&  (topArray[i][1]<gMax)
            &&  (topArray[i][2]>bMin)
            &&  (topArray[i][2]<bMax)
           )||(
                (topArray[i][0]>89)  //check for blue
            &&  (topArray[i][0]<113)
            &&  (topArray[i][1]>gMin-2)
            &&  (topArray[i][2]>bMin-2)
           )||(
                (topArray[i][0]>rMin-5)  //check for light pink
            &&  (topArray[i][0]<rMax)
            &&  (topArray[i][1]>192)
            &&  (topArray[i][1]<222)
            &&  (topArray[i][2]>bMin-5)
            &&  (topArray[i][2]<bMax)
           )){
            frameData[frameDataIndex++] = 0xFF;
            frameData[frameDataIndex++] = 0xFF;
            frameData[frameDataIndex++] = 0xFF;
            frameData[frameDataIndex++] = 0xFF;
        }
        else{
            frameData[frameDataIndex++] = 0x00;
            frameData[frameDataIndex++] = 0x00;
            frameData[frameDataIndex++] = 0x00;
            frameData[frameDataIndex++] = 0xFF;
        }
    }

    //re-encode data to jpeg
    var rawFrameData = {
        data: frameData,
        width: rawImageData.width,
        height: rawImageData.height
    };

    return rawFrameData;
}


function compareToJson(resultJson, imageIndex){
    var badTop = true;
    var badBot = true;
    //console.log("Copying json on "+imageIndex);
    var myResultJson = resultJson;
    resultJson = null;
    var correctJson;
    fs.readFile('./ClashRoyale/names.json', function(err, data){

        correctJson = JSON.parse(data);
        if(myResultJson.topPlayer != correctJson.images[imageIndex].topPlayer){
            console.log("Top miss, trying to correct...");
            if(!tryToCorrect(myResultJson.topPlayer, correctJson.images[imageIndex].topPlayer)){
                console.log("Still miss, getting best match... ")
                console.log("OCR read: "+myResultJson.topPlayer+". correct result was: "+correctJson.images[imageIndex].topPlayer+".");
                getBestMatch(myResultJson.topPlayer, function(result){
                    console.log("Best predicted match is: "+result);
                    if(result!=correctJson.images[imageIndex].topPlayer){
                        topMiss+=1;
                        console.log("Got a top miss: "+topMiss);
                        badTop = false;
                        if(!badTop && !badBot){
                            done = true;
                        }
                    }
                    else{
                        console.log("Corrected with string matching");
                        badTop = false;
                        if(!badTop && !badBot){
                            done = true;
                        }
                    }
                });
            }
            else{
                console.log("Corrected in postprocessing");
                badTop = false;
                if(!badTop && !badBot){
                    done = true;
                }
            }

        }
        else{
            badTop = false;
            if(!badTop && !badBot){
                done = true;
            }
        }
        if(myResultJson.bottomPlayer != correctJson.images[imageIndex].bottomPlayer){
            console.log("Bot miss, trying to correct...");
            if(!tryToCorrect(myResultJson.bottomPlayer, correctJson.images[imageIndex].bottomPlayer)){
                console.log("Still miss, getting best match... ")
                console.log("OCR read: "+myResultJson.bottomPlayer+". correct result was: "+correctJson.images[imageIndex].bottomPlayer+".");
                getBestMatch(myResultJson.bottomPlayer, function(result){
                    console.log("Best predicted match is: "+result);
                    if(result!=correctJson.images[imageIndex].bottomPlayer){
                        botMiss+=1;
                        console.log("Got a bot miss: "+botMiss);
                        badBot = false;
                        if(!badTop && !badBot){
                            done = true;
                        }
                    }
                    else{
                        console.log("Corrected with string matching");
                        badBot = false;
                        if(!badTop && !badBot){
                            done = true;
                        }
                    }
                });

            }
            else{
                console.log("Corrected in postprocessing");
                badBot = false;
                if(!badTop && !badBot){
                    done = true;
                }
            }
        }
        else{
            badBot = false;
            if(!badTop && !badBot){
                done = true;
            }
        }
        if(myResultJson.winner != correctJson.images[imageIndex].winner){
            winMiss+=1;
            console.log("Got a win miss: "+winMiss);
            console.log("OCR read: "+myResultJson.winner+". correct result was: "+correctJson.images[imageIndex].winner+".");
        }
        compareCounter++;
        console.timeEnd("processImage");
        if(compareCounter == numBatch){
            var topAcc = ((1-(topMiss/numBatch))*100);
            var botAcc = ((1-(botMiss/numBatch))*100);
            var winAcc = ((1-(winMiss/numBatch))*100);
            var combinedNameAcc = (topAcc + botAcc)/2;
            var totalCombinedAcc = (topAcc+botAcc+winAcc)/3;
            var accuracyJson = {
                "Top Misses":topMiss,
                "Top Accuracy":topAcc+'%',
                "Bot Misses":botMiss,
                "Bot Accuracy":botAcc+'%',
                "Win Misses":winMiss,
                "Win Accuracy":winAcc+'%',
                "Combined Name Accuracy":combinedNameAcc+'%',
                "Total Combined Accuracy":totalCombinedAcc+'%'
            };
            console.log(accuracyJson);
            //res.json(accuracyJson);
        }
    })
}

function tryToCorrect(inputString, correctString){
    if(inputString == "Unknown name"){
        //this can't be corrected
        return false;
    }

    //try u -> n
    var testString = inputString.replace(/u/g, "n");
    if(testString == correctString){
        console.log("Fixed with u -> n");
        return true;
    }

    //try l -> 1
    var testString2 = inputString.replace(/l/g, "1");
    testString = testString.replace(/l/g, "1");
    if(testString == correctString || testString2 == correctString){
        console.log("Fixed with l -> 1");
        return true;
    }

    //try n -> r
    var testString3 = inputString.replace(/n/g, "r");
    testString2 = testString2.replace(/n/g, "r");
    if(testString2 == correctString || testString3 == correctString){
        console.log("Fixed with n -> r");
        return true;
    }

    //try 0 -> o
    var testString4 = inputString.replace(/0/g, "o");
    testString3 = testString3.replace(/0/g, "o");
    testString2 = testString2.replace(/0/g, "o");
    testString = testString.replace(/0/g, "o");
    if(testString == correctString || testString2 == correctString || testString3 == correctString || testString4 == correctString){
        console.log("Fixed with 0 -> o");
        return true;
    }

}

function getBestMatch(inputString, callback){
    //console.log("getting best match");
    fs.readFile('./ClashRoyale/allNames.json', function(err, data){
        var s1len = inputString.length;
        var names = JSON.parse(data);
        //console.log("name data is: "+names);
        //console.log("first name is: "+names[0]);
        var lowestValue = 999; //something crazy high
        var maxNumberOfChildren = names.length;
        var childResponses = [];
        var childrenPIDs = [];
        var numResponses = 0;
        var guessString= "";
        //spin up children
        for(var i =0; i<names.length; i++){
            var tempIndex = i;
            var s2 = names[tempIndex];
            var s2len = s2.length;
            var childResponses = [];
            var child = cp.fork('./sift4', [inputString, s1len, s2, s2len]);
            childrenPIDs.push(child.pid);
            child.send('execute');
            //listen for childResponses
            child.on('message', function(response){
                childResponses.push(response);
                //recieved a response
                numResponses++;
                //if we have all responses
                if(numResponses == maxNumberOfChildren){
                    for(var j =0; j<childResponses.length; j++){
                        //console.log("Dist for "+childResponses[j].string+" is: "+childResponses[j].dist);
                        if(childResponses[j].dist < lowestValue){
                            //console.log("reseting lowest index to "+i);
                            lowestValue = childResponses[j].dist;
                            guessString = childResponses[j].string;
                        }
                    }
                    for(var j =0; j<childrenPIDs.length; j++){
                        terminate(childrenPIDs[j]);
                    }
                    callback(guessString);
                }
            });
        }
    })
}

function ldist(s1, s1len, s2, s2len){
    var cost;
    if(s1len == 0) return s1len;
    if(s2len == 0) return s2len;

    if(s1[s1len-1] == s2[s2len-1]){
        cost =0;
    }
    else{
        cost=1;
    }
    return Math.min( ldist(s1,s1len-1,s2,s2len)+1,
                     ldist(s1,s1len,s2,s2len-1)+1,
                     ldist(s1,s1len-1,s2,s2len-1)+cost);
}

function sift4simple(s1, s2, maxOffset) {
    if (!s1||!s1.length) {
        if (!s2) {
            return 0;
        }
        return s2.length;
    }

    if (!s2||!s2.length) {
        return s1.length;
    }

    var l1=s1.length;
    var l2=s2.length;

    var c1 = 0;  //cursor for string 1
    var c2 = 0;  //cursor for string 2
    var lcss = 0;  //largest common subsequence
    var local_cs = 0; //local common substring

    while ((c1 < l1) && (c2 < l2)) {
        if (s1.charAt(c1) == s2.charAt(c2)) {
            local_cs++;
        } else {
            lcss+=local_cs;
            local_cs=0;
            if (c1!=c2) {
                c1=c2=Math.max(c1,c2); //using max to bypass the need for computer transpositions ('ab' vs 'ba')
            }
            for (var i = 0; i < maxOffset && (c1+i<l1 || c2+i<l2); i++) {
                if ((c1 + i < l1) && (s1.charAt(c1 + i) == s2.charAt(c2))) {
                    c1+= i;
                    local_cs++;
                    break;
                }
                if ((c2 + i < l2) && (s1.charAt(c1) == s2.charAt(c2 + i))) {
                    c2+= i;
                    local_cs++;
                    break;
                }
            }
        }
        c1++;
        c2++;
    }
    lcss+=local_cs;
    return Math.round(Math.max(l1,l2)- lcss);
}

module.exports.processImage = processImage;
module.exports.testBatch = testBatch;
module.exports.ldist = ldist;
