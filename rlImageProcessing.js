var microsoftAPI = require('microsoft-computer-vision');
var fs = require('fs');
var im = require('imagemagick');
var jpeg = require('jpeg-js');

var topNameCropParams = '400x200+1006+476';
var botNameCropParams = '400x200+1006+785';
var topScoreCropParams = '520x200+1495+476';
var botScoreCropParams = '520x200+1495+785';
var cropParams = [topNameCropParams, topScoreCropParams, botNameCropParams, botScoreCropParams];


function processImage(res){
    im.resize({
        srcPath: './RocketLeague/rl_images1.jpg',
        dstPath: './RocketLeague/test.jpg',
        width:   2560,
        quality: 1,
        format: 'jpg'
    }, function(err, stdout, stderr){
        if (err) throw err;
        im.convert(['./RocketLeague/test.jpg', '-crop', cropParams[1], './RocketLeague/test.jpg'], function(err, stdout){
            if(err) throw err;
            im.resize({
                srcPath: './RocketLeague/test.jpg',
                dstPath: './RocketLeague/test.jpg',
                width:   800,
                quality: 1,
                format: 'jpg'
            }, function(err, stdout, stderr){
                fs.readFile('./RocketLeague/test.jpg', function(err, data){
                    var rawImageData = jpeg.decode(data);
                    var rawFrameData = getFrameData(rawImageData);
                    var jpegImageData = jpeg.encode(rawFrameData, 50);

                    fs.writeFile('./RocketLeague/processedTest.jpg', jpegImageData.data, function(err){
                        if(err) throw err;
                        microsoftAPI.orcImage({
                            "Ocp-Apim-Subscription-Key": "ff4773cc53004fb3b77c6f247c7cf25e",
                            "request-origin":"westcentralus",
                            "detect-orientation":false,
                            "language":"en",
                            "content-type":"application/octet-stream",
                            "body":jpegImageData.data
                        }).then((result) =>{
                            var less = result.regions[0];
                            // for(var i=0; i<less.lines.length; i++){
                            //     var line = '';
                            //     for(var j=0; j<less.lines[i].words.length; j++){
                            //         line+=less.lines[i].words[j].text;
                            //     }
                            //     console.log(line);
                            // }
                            res.json(less);
                        })
                    })
                })
            })
        })
    });
}

function getFrameData(rawImageData){
    //build 2d array from image data;
    var topArray = [];
    for(var i =0; i<rawImageData.data.length; i+=0){
        var lowerArray = [];
        for(var j=0; j<4; j++){
            if(j==3){
                topArray.push(lowerArray);
                i++;
            }
            else{
                lowerArray[j]=rawImageData.data[i];
                i++;
            }
        }
    }

    //collect all points where RBG is within threshhold and set to 255, all other points 0
    var rMin = 32;
    var rMax = 252;
    var gMin = 72;
    var gMax = 256;
    var bMin = 98;
    var bMax = 256;
    var frameData = new Buffer(rawImageData.width * rawImageData.height * 4);
    var frameDataIndex =0;
    for(var i=0; i<topArray.length; i++){
        //check if this RBG aray is within threshhold
        if((
                (topArray[i][0]>rMin)
            &&  (topArray[i][0]<rMax)
            &&  (topArray[i][1]>gMin)
            &&  (topArray[i][1]<gMax)
            &&  (topArray[i][2]>bMin)
            &&  (topArray[i][2]<bMax)
            &&  ((topArray[i][2]+5)>topArray[i][1])
            &&  ((topArray[i][2]+5)>topArray[i][0])
           )
        //    ||(
        //         (topArray[i][0]>89)  //check for blue
        //     &&  (topArray[i][0]<113)
        //     &&  (topArray[i][1]>gMin-2)
        //     &&  (topArray[i][2]>bMin-2)
        //    )||(
        //         (topArray[i][0]>rMin-5)  //check for light pink
        //     &&  (topArray[i][0]<rMax)
        //     &&  (topArray[i][1]>192)
        //     &&  (topArray[i][1]<222)
        //     &&  (topArray[i][2]>bMin-5)
        //     &&  (topArray[i][2]<bMax)
        //    )
       ){
            frameData[frameDataIndex++] = 0xFF;
            frameData[frameDataIndex++] = 0xFF;
            frameData[frameDataIndex++] = 0xFF;
            frameData[frameDataIndex++] = 0xFF;
        }
        else{
            frameData[frameDataIndex++] = 0x00;
            frameData[frameDataIndex++] = 0x00;
            frameData[frameDataIndex++] = 0x00;
            frameData[frameDataIndex++] = 0xFF;
        }
    }

    //re-encode data to jpeg
    var rawFrameData = {
        data: frameData,
        width: rawImageData.width,
        height: rawImageData.height
    };

    return rawFrameData;
}

module.exports.processImage = processImage;
